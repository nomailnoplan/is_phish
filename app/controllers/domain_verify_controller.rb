class DomainVerifyController < ApplicationController
  def check
    result = verified_domains.include?(url_params[:url])

    render json: { verified: result }
  end

  private

  def url_params
    params.permit(:url)
  end

  def verified_domains
    # memoizing / caching urls for performance
    # using a set here also for performance
    @verified_domains ||=
      Set.new(PhishTankReference.where(verified: true).pluck(:url).sort)
  end
end
