Rails.application.routes.draw do
  get '/check', to: 'domain_verify#check', defaults: { format: :json }
end
