class CreatePhishTankReferences < ActiveRecord::Migration[5.2]
  def change
    create_table :phish_tank_references do |t|
      t.integer :phish_id, null: false
      t.string :url, null: false
      t.string :phish_detail_url, null: false
      t.datetime :submission_time, null: false
      t.boolean :verified
      t.datetime :verification_time
      t.boolean :online
      t.string :target
      t.string :ip_address
      t.string :cidr_block
      t.string :announcing_network
      t.string :rir
      t.datetime :detail_time
    end
  end
end
