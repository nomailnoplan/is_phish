require 'csv'

path = File.expand_path('./data/verified_online.csv', __dir__)

CSV.read(path, headers: true).each do |l|
  PhishTankReference.create(
    phish_id: l['phish_id'],
    url: l['url'],
    phish_detail_url: l['phish_detail_url'],
    submission_time: l['submission_time'],
    verified: l['verified'] == 'yes' ? true : false,
    verification_time: l['verification_time'],
    online: l['online'] == 'yes' ? true : false,
    target: l['target'],
  )
end
