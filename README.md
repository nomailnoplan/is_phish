## Setup
* Install ruby 2.5.1 using the method of your choice
(e.g, https://cache.ruby-lang.org/pub/ruby/2.5/ruby-2.5.1.tar.gz or https://github.com/rbenv/rbenv)
* Once ruby is built and / or installed, using ruby 2.5.1
(check you are using the correct ruby with `ruby -v`)run `gem install bundler`
* Change directory to the project root `cd is_phish`
* Run `bundle install`
* Run `bundle exec rails db:migrate`
* Run `bundle exec rails db:seed` to seed the database with the PhishTank data
* Run the app server `bundle exec rails s`
* To test the API endpoint you may hit it in your browser or with curl:
`curl http://localhost:3000/check?url=http://www.paypal.com.cgi-bin.webscr.ikubiak.webd.pl/`
* The first request may be a bit slow, but once the cache is warmed the endpoint is quite performant
